
#TP4 INLO
#Programme de test construisant et affichant des polygones de diverses sortes et permettant de vérifier que la méthode aire est correcte
#Zerva Kontou Marina


import math
import TP4_inlo_heritance.py


#Points à coordonées cartésiennes:
i1=Point(1,2)
i2=Point(3,4)
i3=Point(5,8)
i4=Point(10,14)



#creation d'objet Polygone
polygone=Polygone([i1,i2])   #Polygone prend comme argument une liste de points
print(polygone.aire())

#creation d'objet Triangle  
triangle=Triangle(i1,i2,i3)    #Triangle prend comme arguments des points
print(triangle.aire())

#creation d'objet Polygone Regulier    
pg=PolygoneRegulier([i1,i2,i3,i4]0,6,8)     #Polygone prend comme argument entre autres une liste se points
print(pg.aire())


#creation d'objet Rectangle         
rectangle=Rectangle(i1,i2,i3,i4)  #Rectangle prend comme arguments des points
print(rectangle.aire())





