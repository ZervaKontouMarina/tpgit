
#Zerva Kontou Marina

#Exécutez : python3 -m unittest test_random.py -v

-Combien de tests échouent, combien de tests crashent ? Pourquoi ?

#Il y a un test qui echoue( test_choice_erroneous_test) et un test qui crache(test_crashing_test).
#Le test_choice_erroneous_test echoue car "elt" ( l'élement) n'est pas dans ('a', 'b', 'c') (méthode utilisée: assertIn).
#Le test_crashing_test crache car 6 n' est pas égal a 6/0 (& on ne peut pas diviser par zero), ici la méthode utilisée est "assertEqual".


-Notez les deux façons de tester qu’un code lève une Exception.

#Methode 1: assertRaises(IndexError, fonction, *args, **kwargs)
#Méthode 2: try...except

-Cherchez sur internet le rôle des méthodes setUp et tearDown


#Ces deux méthodes assurent que chaque méthode de test dans la classe de test (Dans ce cas "RandomTest", qui hérite de unittest.TestCase), a subit correctement le setUp et le tearDown.
#Plus précisément, la méthode "setUp" sert à initialiser les test et celle du "tearDown" libère les ressources et ferme les fichiers ouverts dans le setUp.





